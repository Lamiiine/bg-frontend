import React, { useState } from "react";
import axios from "axios";
import "./search.css";

const Search = () => {
    const [query, setQuery] = useState("");
    const [results, setResults] = useState([]);

    const handleSearch = async() => {
        try {
            const response = await axios.post("http://localhost:8000/generate/", {
                prompt: query,
            });
            setResults([response.data.generated_text]);
        } catch (error) {
            console.error("Error generating text:", error);
        }
    };

    return ( <
        div className = "center-container" >
        <
        div className = "search-container" >
        <
        input className = "search-input"
        type = "text"
        value = { query }
        onChange = {
            (e) => setQuery(e.target.value)
        }
        placeholder = "Ask BIGmama..." /
        >
        <
        button className = "search-button"
        onClick = { handleSearch } >
        Search <
        /button> < /
        div > <
        div className = "results" > {
            results.map((result, index) => ( <
                div className = "result-item"
                key = { index } >
                <
                p > { result } < /p> < /
                div >
            ))
        } <
        /div> < /
        div >
    );
};

export default Search;